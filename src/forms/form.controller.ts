import { Body, Controller, Post } from "@nestjs/common";
import { CreateFormDto } from "./dtos/create-form-dto";
import { FormService } from "./form.service";

@Controller('forms')
export class FormController {
    constructor(private readonly formService: FormService) {}

    @Post()
    create(@Body() dto: CreateFormDto) {
        return this.formService.create(dto);
    }
}