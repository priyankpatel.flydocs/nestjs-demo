import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CreateFormDto } from "./dtos/create-form-dto";
import { Form } from "./form.entity";

@Injectable()
export class FormService {
    constructor(@InjectRepository(Form) private readonly formRepository: Repository<Form>) {}

    async create(dto: CreateFormDto) {
        const form = this.formRepository.create(dto);
        return await this.formRepository.save(form);
    }

    // async findAll() {
    //     return await this.formRepository.find({
    //         relations: {
    //             form_data: true,
    //         }
    //     })
    // }
}