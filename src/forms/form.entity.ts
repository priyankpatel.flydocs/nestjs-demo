import { FormValue } from 'src/form_value/formValue.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn } from 'typeorm';


@Entity({ name : 'forms'})
export class Form {
    @PrimaryGeneratedColumn()
    id:number;

    @Column({unique: true})
    title: string;

    @Column()
    form_value: string;

    @OneToMany(()=> FormValue, (formValue) => formValue.formId)
    form_data: FormValue[];
}
