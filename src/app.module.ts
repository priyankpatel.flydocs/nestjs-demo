import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Form } from './forms/form.entity';
import { FormModule } from './forms/form.module';
import { FormValue } from './form_value/formValue.entity';
import { FormValueModule } from './form_value/formValue.module';


@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.get('DB_HOST'),
        port: +configService.get('DB_PORT'),
        username: configService.get('DB_USERNAME'),
        password: configService.get('DB_PASSWORD'),
        database: configService.get('DB_DATABASE'),
        entities: [Form, FormValue],
        synchronize: true,
      }),
      inject: [ConfigService],
    }),
    FormModule,
    FormValueModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
