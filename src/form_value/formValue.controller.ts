import { Body, Controller, Get, Post } from "@nestjs/common";
import { CreateFormValueDto } from "./dtos/create-form-value-dto";
import { FormValueService } from "./formValue.service";

@Controller('form-value')
export class FormValueController {
    constructor(private readonly formValueService: FormValueService) {}

    @Post()
    create(@Body() dto: CreateFormValueDto) {
        return this.formValueService.create(dto);
    }

    @Get()
    findAll() {
        return this.formValueService.findAll();
    }

    @Get('form-title')
    findFilterFormAll() {
        return this.formValueService.findAll();
    }

}