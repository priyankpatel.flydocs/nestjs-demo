import { Form } from 'src/forms/form.entity';
import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, OneToOne, ManyToOne, LineString } from 'typeorm';


@Entity({ name : 'form_values'})
export class FormValue {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    uu_id:string;

    @Column({name: 'form_id'})
    formId: number;

    @ManyToOne(()=> Form, (form) => form.title)
    @JoinColumn({ name: 'form_id'})
    form_name: Form;

    @Column()
    form_filed: string;

    @Column()
    form_filed_value:string;
}
