export class CreateFormValueDto {
    uu_id: string;
    form_id: number;
    form_filed: string;
    form_filed_value: string;
}