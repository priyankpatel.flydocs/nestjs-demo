import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { FormValueController } from "./formValue.controller";
import { FormValue } from "./formValue.entity";
import { FormValueService } from "./formValue.service";

@Module({
 imports: [TypeOrmModule.forFeature([FormValue])],
 controllers: [FormValueController],
 providers: [FormValueService],
})
export class FormValueModule{}