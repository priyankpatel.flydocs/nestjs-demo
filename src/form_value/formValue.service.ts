import { Body, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { randomUUID } from "crypto";
import { Repository } from "typeorm";
import { CreateFormValueDto } from "./dtos/create-form-value-dto";
import { FormValue } from "./formValue.entity";

@Injectable()
export class FormValueService {
    constructor(@InjectRepository(FormValue) private readonly formValueRepository: Repository<FormValue>) {}

    async create(dto: CreateFormValueDto) {
        const uuid = randomUUID();
        const form = this.formValueRepository.create(dto);
        form['uu_id'] = uuid;
        return await this.formValueRepository.save(form);
    }

    async findAll() {
        return await this.formValueRepository.find({
            relations: {
                form_name: true,
            }
        })
    }
}