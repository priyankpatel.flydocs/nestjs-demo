<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

# Create .env file and configure database from reference of .env.example file

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev


## Test

Api :

1. Create Form Api:
   Url : http://localhost:3000/forms
   Method: POST
   Request Param : 
   {
	"title":"form3",
	"form_value":"{name:string,email:email,phonenumber:number}"
   }
   Resposce : 
   {
	"title": "form3",
	"form_value": "{name:string,email:email,phonenumber:number}",
	"id": 1
   }
				   
2. Insert Form Values Api:
   Url : http://localhost:3000/form-value
   Method: POST
   Request Param : 
   {
	"formId":1,
	"form_filed":"name",
	"form_filed_value":"test2"
   }
   Resposce :
   {
	"formId": 1,
	"form_filed": "name",
	"form_filed_value": "test2",
	"uu_id": "aa6d807d-1e25-40eb-a3ae-84885517f942",
	"id": 4
   }

3. Get Form Values Api:
   Url : http://localhost:3000/form-value
   Method: Get
   Request Param : ""
   Resposce : 
   [
	{
		"id": 1,
		"formId": 1,
		"form_filed": "name",
		"form_filed_value": "test",
		"form_name": {
			"id": 1,
			"title": "form3",
			"form_value": "{name:string,email:email,phonenumber:number}"
		}
	},
	{
		"id": 2,
		"formId": 1,
		"form_filed": "email",
		"form_filed_value": "email@yml.com",
		"form_name": {
			"id": 1,
			"title": "form3",
			"form_value": "{name:string,email:email,phonenumber:number}"
		}
	}
  ]


